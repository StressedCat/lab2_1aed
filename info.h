#include <iostream>
using namespace std;

#ifndef INFO_H
#define INFO_H

class info {
    private:
      int x;
      int tam;

    public:
      /* constructores */
      info();
      info(int x, int tam);

      /* set y get */
      int get_x();
      int get_tam();
      void set_x(int x);
      void set_tam(int tam);

      /* funcion de informacion */
      void Info();
};
#endif
