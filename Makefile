prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = main.cpp info.cpp
OBJ = main.o info.o
APP = Pilas

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
