#include <iostream>
using namespace std;
#include "info.h"


int Add(int *pila, int x, int tam){
  /*
   * En el caso que el tamaño sea igual a la posicion donde se agregara
   * el siguiente numero, la operacion sera cancelada, junto con decir la
   * misma razon de porque no se puede, caso contrario, se le pedirá al usuario
   * que ingrese el valor que desea, si o si debe ser un int
   */
  if (x < tam) {
    int num;
    cout << "Ingrese el valor que desea agregar: ";
    cin >> num;
    pila[x] = num;
    x = x + 1;
  }
  else if (x == tam){
    cout << "La pila ya esta en su capacidad maxima" << endl;
  }
  return x;
}

int Remove(int *pila, int x) {
  /*
   * En el  caso que al eliminar un valor, la posicion x sea igual a 0, la
   * operacion será cancelada junto con decir la razon de porque fue cancelada,
   * caso contrario, se le dira al usuario que valor fue eliminado, el valor
   * eliminado siempre será el mas reciente o el de la 'punta', ya que se esta
   * simulando una pila
   */
  if (x != 0) {
    cout << "El numero " << pila[x-1] << " fue eliminado de la pila" << endl;
    pila[x] = 0;
    x = x - 1;
  }
  else if (x == 0){
    cout << "La pila ya esta vacia" << endl;
  }
  return x;
}

void Show(int *pila, int x){
  /*
   * Si el usuario ingresa esta opcion, se le mostrara la pila con el orden
   * desde el mas reciente hasta el mas viejo, con el objetivo de simular la
   * pila, mostrandose cada una encima de la otra para orientar al usuario
   */
  for (int i=x-1; i>=0; i--){
    cout << pila[i] << endl;
  }
}

int main() {
  /*
   * Se le pedira al usuario que ingrese cual es el tamaño en su pila.
   * Este tamaño no podra ser cambiado y será el limitante de cuanto sera el
   * maximo de la pila
   * Existen varias opciones, add consiste en añadir, un numero a la pila
   * remove es para eliminar el valor en el tope de la pila
   * Show es para ver la pila, mostrando del mas arriba hasta al de abajo
   * Info es para ver la informacion, dira el tamaño maximo, posicion y
   * capacidad restante de la pila
   */
  bool ShutDown = false;
  int opt;
  int x = 0;
  int tam;
  cout << "Ingrese el tamaño maximo de su pila: ";
  cin >> tam;
  int pila[tam];
  info i = info(x, tam);
  while (ShutDown == false) {
    cout << "[1] Para añadir" << endl << "[2] para eliminar" << endl;
    cout << "[3] Para inspeccionar" << endl << "[4] para info basica" << endl;
    cout << "[0] Salir del programa" << endl << "Ingrese su opcion: ";
    cin >> opt;
    cout << "******************************************************" << endl;
    if (opt == 1) {
      x = Add(pila, x, tam);
    }
    else if(opt == 2){
      x = Remove(pila, x);
    }
    else if(opt == 3){
      Show(pila, x);
    }
    else if(opt == 4){
      i.Info();
    }
    else if(opt == 0) {
      ShutDown = true;
      cout << "El programa fue apagado" << endl;
    }
    else{
      cout << "opcion incorrecta" << endl;
    }
    i.set_x(x);
    cout << "******************************************************" << endl;
  }
}
