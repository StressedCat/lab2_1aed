#include <iostream>
using namespace std;
#include "info.h"

/* constructores */
info::info(){
  int x;
  int tam;
}

info::info(int x, int tam){
  this->x = x;
  this->tam = tam;
}

/* get and set */
int info::get_x(){
  return this->x;
}

int info::get_tam(){
  return this->tam;
}

void info::set_x(int x){
  this->x = x;
}

void info::set_tam(int tam){
  this->tam = tam;
}

/* Esta funcion sera usada para mostrar la informacion basica de la pila */
void info::Info(){
  int posicion = this->x;
  int tam = this->tam;
  cout << "la posicion para el siguiente valor es: " << posicion << endl;
  cout << "el tamaño maximo de la pila es: " << tam << endl;
  cout << "capacidad restante: " << tam-x << endl;
}
