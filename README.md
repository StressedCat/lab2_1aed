# Lab2_1AED
Para iniciar el programa:
1) Ingrese en el CMD el comando make para instalar el programa
-> .../lab2_1aed$ make

2) Se creará un programa con nombre de Pilas, para ingresar a este
   haga lo siguiente:
-> .../lab2_1aed$ ./Pilas


En el programa:
1) El programa esta creado para mostrar como funciona una pila:

2) Como usuario, usted debe agregar cuantos el tamanio maximo de la pila, junto
   con esto, debe agregar los numeros que desea agregar en la pila, solo se
   pueden int.

3) Al ingresar los datos, si presiona el boton 3, podra ver los numeros en
   forma de pila, ademas, si presiona 4, podra ver la informacion de la pila,
   como la capacidad, el tamaño que usted agrego o la posicion del siguiente
   numero que va a agregar

Para borrar el programa:
1) Asegurese que esté en la misma carpeta donde corrio el programa
-> .../lab2_1aed/

2) Ingrese el comando make clean para borrar el programa que uso
-> .../lab2_1aed$ make clean
